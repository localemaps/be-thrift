namespace java com.localemaps.be
#@namespace scala com.localemaps.be

/**
 * NOTE: Cannot be used by Scrooge (see https://github.com/twitter/scrooge/issues/165)
 */
enum DayOfWeek {
  SUNDAY,
  MONDAY,
  TUESDAY,
  WEDNESDAY,
  THURSDAY,
  FRIDAY,
  SATURDAY
}

/**
 * NOTE: Cannot be used by Scrooge (see https://github.com/twitter/scrooge/issues/165)
 */
enum DistanceUnit {
  KILOMETERS,
  MILES
}

struct Contact {
  1: optional string email,
  2: optional string tel
}

struct Country {
  1: string iso2,
  20: optional string name
}

struct Distance {
  1: double value,
  /* TODO: Change to DayOfWeek when issue is resolved. */
  2: string unit
}

struct Point {
  1: double latitude,
  2: double longitude
}

struct Bounds {
  1: Point northeast,
  2: Point southwest
}

struct Location {
  1: Point point,
  2: bool isApproximate
}

struct Locale {
  1: string name,
  2: string streetAddress,
  3: string locality,
  4: string region,
  5: string postalCode,
  6: i64 timestamp,
  7: Country country,
  8: Location location,
  20: optional i64 id,
  21: optional string extendedAddress,
  22: optional string shortName,
  23: optional Contact contact,
  24: optional i64 dedication
}

struct NavItem {
  1: string name,
  20: optional i64 id,
  21: optional i64 parent,
  22: optional i64 locale,
  23: optional Bounds bounds
}

struct Time {
  1: byte hours,
  2: byte minutes
}

struct WorshipService {
  /* TODO: Change to DayOfWeek when issue is resolved. */
  1: byte day,
  2: Time time,
  3: bool isCws = false,
  20: optional i64 id,
  21: optional string language
}

struct Notice {
  2: string description,
  3: i64 startDate,
  4: i64 endDate,
  20: optional i64 id
}

struct MapData {
  1: bool exactMatch,
  20: optional Distance radius,
  21: optional Point center,
  22: optional string address,
  23: optional string country
}

struct SearchResult {
  1: Locale locale,
  2: Distance distance
}

struct SearchResultSet {
  1: string query,
  20: optional MapData mapData,
  21: optional list<SearchResult> results
}

service BackendService {
  i64 createLocale(1:Locale locale, 2:i64 navItemParent),
  void removeLocale(1:i64 id),
  Locale retrieveLocale(1:i64 id),
  Locale retrieveLocaleByPath(1:string path),
  list<Locale> retrieveLocalesByBounds(1:Point northeast, 2:Point southwest),
  SearchResultSet retrieveLocalesBySearch(1:string query, 2:Point center, 3:Distance maxDistance),
  i64 createNavItem(1:NavItem navItem),
  void removeNavItem(1:i64 id),
  NavItem retrieveNavItem(1:i64 id),
  list<NavItem> retrieveNavItemRegions(),
  list<NavItem> retrieveNavItemsByParent(1:i64 id),
  void updateNavItem(1:NavItem navItem),
  void updateLocale(1:Locale locale),
  i64 createWorshipService(1:WorshipService svc, 2:i64 localeId),
  void removeWorshipService(1:i64 id),
  list<WorshipService> retrieveWorshipServices(1:i64 localeId),
  void updateWorshipService(1:WorshipService svc),
  i64 createNotice(1:Notice notice, 2:i64 localeId),
  void removeNotice(1:i64 id),
  list<Notice> retrieveNotices(1:i64 localeId, 2:i64 time),
  void updateNotice(1:Notice notice)
}
